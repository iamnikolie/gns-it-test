<?php

class Model
{

    protected $configures;

    protected $tablename;
    protected $allowed;

    protected $connection;

    function __construct(){
        $this->configures = imnCore::$sets['db'];
        $this->connection = $this->connect();
    }

    private function connect(){
        return new PDO('mysql:host='.$this->configures['host'].';dbname='.$this->configures['db'],
            $this->configures['user'], $this->configures['pass']);
    }

    public function all(){
        $stmt = $this->connection->query("SELECT * FROM ".$this->tablename." WHERE isActive='1'");
        $results = array();
        while ($row = $stmt->fetchObject())
        {
            array_push($results, $row);
        }
        return $results;
    }

    public function delete($id){
        $sql = "DELETE FROM ".$this->tablename." WHERE id =  :id";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function save($inserts) {
        $sql = "INSERT INTO ".$this->tablename." SET ".$this->pdoSet($this->allowed, $inserts);
        $stm = $this->connection->prepare($sql);
        $stm->execute($inserts);
    }

    protected function pdoSet($allowed, &$values, $source = array()) {
        $set = '';
        $values = array();
        if (!$source) $source = &$_POST;
        foreach ($allowed as $field) {
            if (isset($source[$field])) {
                $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
                $values[$field] = $source[$field];
            }
        }
        return substr($set, 0, -2);
    }

}