<?php

require_once ('Model.php');

class Film extends Model {

    protected $tablename = 'films';
    protected $allowed = array('name', 'year', 'isActive');

}