<?php
/* Smarty version 3.1.28, created on 2016-01-28 02:54:25
  from "/var/www/gns-it/templates/add.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56a982e1526ab7_78592530',
  'file_dependency' => 
  array (
    '661e0f2fd1beb73f2b07ff9d5bffe7c3d0e864f3' => 
    array (
      0 => '/var/www/gns-it/templates/add.tpl',
      1 => 1453949662,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56a982e1526ab7_78592530 ($_smarty_tpl) {
?>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Добавить новый фильм</h2>
                <hr>
                <form method="POST" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="name" class="control-label">Название</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="name" class="control-label">Год выпуска</label>
                        </div>
                        <div class="col-md-4">
                            <input type="number" min="1800" class="form-control" name="year">
                        </div>
                        <div class="col-md-4 text-center">
                            <label for="isActive" class="control-label">Активен &nbsp; <input type="checkbox" name="isActive" checked></label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <input type="submit" value="Добавить" name="submit" id="add-form-submit" class="btn btn-block btn-success">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-head"></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="index.php" class="list-group-item">
                                Все фильмы
                            </a>
                            <a href="/?controller=home&action=add" class="list-group-item active">
                                Добавить фильм
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> API CODE </h4>
            </div>
            <div class="modal-body" id="getCode" >
                //ajax success content here.
            </div>
        </div>
    </div>
</div><?php }
}
