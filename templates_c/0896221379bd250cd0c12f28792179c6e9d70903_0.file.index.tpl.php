<?php
/* Smarty version 3.1.28, created on 2016-01-28 05:20:01
  from "/home/holod00/imn.co.ua/www/gns-it/templates/index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56a988e1028e16_49896375',
  'file_dependency' => 
  array (
    '0896221379bd250cd0c12f28792179c6e9d70903' => 
    array (
      0 => '/home/holod00/imn.co.ua/www/gns-it/templates/index.tpl',
      1 => 1453951187,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56a988e1028e16_49896375 ($_smarty_tpl) {
?>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Все фильмы</h2>
                <hr>
                <table class="table table-striped table-hover">
                    <tr>
                        <td>
                            Название
                        </td>
                        <td>
                            Год выпуска
                        </td>
                        <td style="width: 75px" class="text-center">
                            Опции
                        </td>
                    </tr>
                    <?php
$_from = $_smarty_tpl->tpl_vars['films']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value->year;?>

                            </td>
                            <td style="width: 75px" class="text-center">
                                <a href="?controller=home&action=trash&id=<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
                    <tr></tr>
                </table>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-head"></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['siteurl']->value;?>
/" class="list-group-item active">
                                Все фильмы
                            </a>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['siteurl']->value;?>
/home/add/" class="list-group-item">
                                Добавить фильм
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
