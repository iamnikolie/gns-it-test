<?php
/* Smarty version 3.1.28, created on 2016-01-28 05:23:19
  from "/home/holod00/imn.co.ua/www/gns-it/templates/layout.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56a989a73b0fb6_92619735',
  'file_dependency' => 
  array (
    'f510071fcb66ca56c97a8cf64612fc1ef37b3c8e' => 
    array (
      0 => '/home/holod00/imn.co.ua/www/gns-it/templates/layout.tpl',
      1 => 1453951398,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56a989a73b0fb6_92619735 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $_smarty_tpl->tpl_vars['sitename']->value;?>
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['siteurl']->value;?>
/assets/css/main.css">

</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">GNS IT Test Task</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo $_smarty_tpl->tpl_vars['siteurl']->value;?>
/">Home</a></li>
        </ul>
    </div>
</nav>

<?php if (($_smarty_tpl->tpl_vars['content']->value)) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['content']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }?>

<nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container-fluid">
    </div>
</nav>

<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-2.2.0.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
> var siteurl = '<?php echo $_smarty_tpl->tpl_vars['siteurl']->value;?>
';<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteurl']->value;?>
/assets/js/main.js"><?php echo '</script'; ?>
> 

</body>
</html><?php }
}
