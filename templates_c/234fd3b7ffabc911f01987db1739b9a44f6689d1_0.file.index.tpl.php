<?php
/* Smarty version 3.1.28, created on 2016-01-28 03:07:36
  from "/var/www/gns-it/templates/index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56a985f8496730_52730918',
  'file_dependency' => 
  array (
    '234fd3b7ffabc911f01987db1739b9a44f6689d1' => 
    array (
      0 => '/var/www/gns-it/templates/index.tpl',
      1 => 1453950452,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56a985f8496730_52730918 ($_smarty_tpl) {
?>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Все фильмы</h2>
                <hr>
                <table class="table table-striped table-hover">
                    <tr>
                        <td>
                            Название
                        </td>
                        <td>
                            Год выпуска
                        </td>
                        <td style="width: 75px" class="text-center">
                            Опции
                        </td>
                    </tr>
                    <?php
$_from = $_smarty_tpl->tpl_vars['films']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value->year;?>

                            </td>
                            <td style="width: 75px" class="text-center">
                                <a href="?controller=home&action=trash&id=<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
                    <tr></tr>
                </table>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-head"></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="index.php" class="list-group-item active">
                                Все фильмы
                            </a>
                            <a href="index.php?controller=home&action=add" class="list-group-item">
                                Добавить фильм
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
