<?php
/* Smarty version 3.1.28, created on 2016-01-28 01:56:53
  from "/var/www/gns-it/templates/main.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56a97565b80427_22265137',
  'file_dependency' => 
  array (
    '186268d45c8a51e599190df87f718b48ac038bdd' => 
    array (
      0 => '/var/www/gns-it/templates/main.tpl',
      1 => 1453946209,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56a97565b80427_22265137 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $_smarty_tpl->tpl_vars['sitename']->value;?>
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['siteurl']->value;?>
/assets/css/main.css">

</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">GNS IT Test Task</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 2</a></li>
            <li><a href="#">Page 3</a></li>
        </ul>
    </div>
</nav>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped table-hover">
                    <tr>
                        <td>
                            Название
                        </td>
                        <td>
                            Год выпуска
                        </td>
                    </tr>
                    <?php
$_from = $_smarty_tpl->tpl_vars['films']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value->year;?>

                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
                    <tr></tr>
                </table>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-head"></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="index.php" class="list-group-item active">
                                Все фильмы
                            </a>
                            <a href="index.php?controller=home&action=add" class="list-group-item">
                                Добавить фильм
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container-fluid">
    </div>
</nav>

<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-2.2.0.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"><?php echo '</script'; ?>
>

</body>
</html><?php }
}
