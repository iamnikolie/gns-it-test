
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Все фильмы</h2>
                <hr>
                <table class="table table-striped table-hover">
                    <tr>
                        <td>
                            Название
                        </td>
                        <td>
                            Год выпуска
                        </td>
                        <td style="width: 75px" class="text-center">
                            Опции
                        </td>
                    </tr>
                    {foreach $films as $item}
                        <tr>
                            <td>
                                {$item->name}
                            </td>
                            <td>
                                {$item->year}
                            </td>
                            <td style="width: 75px" class="text-center">
                                <a href="?controller=home&action=trash&id={$item->id}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    <tr></tr>
                </table>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-head"></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="{$siteurl}/" class="list-group-item active">
                                Все фильмы
                            </a>
                            <a href="{$siteurl}/home/add/" class="list-group-item">
                                Добавить фильм
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>