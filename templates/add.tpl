
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Добавить новый фильм</h2>
                <hr>
                <form method="POST" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="name" class="control-label">Название</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="name" class="control-label">Год выпуска</label>
                        </div>
                        <div class="col-md-4">
                            <input type="number" min="1800" class="form-control" name="year">
                        </div>
                        <div class="col-md-4 text-center">
                            <label for="isActive" class="control-label">Активен &nbsp; <input type="checkbox" name="isActive" checked></label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <input type="submit" value="Добавить" name="submit" id="add-form-submit" class="btn btn-block btn-success">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-head"></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="{$siteurl}/" class="list-group-item">
                                Все фильмы
                            </a>
                            <a href="{$siteurl}/home/add/" class="list-group-item active">
                                Добавить фильм
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body text-center" id="getCode" >
                Фильм успешно добавлен!
            </div>
        </div>
    </div>
</div>