<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{$sitename}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{$siteurl}/assets/css/main.css">

</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">GNS IT Test Task</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="{$siteurl}/">Home</a></li>
        </ul>
    </div>
</nav>

{if ($content)}
    {include "$content.tpl"}
{/if}

<nav class="navbar navbar-default navbar-fixed-bottom" id="navbar-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <p class="navbar-text navbar-right">&copy; <a href="mailto:mykola.klitovchenko@gmail.com">Mykola Klitovchenko</a> 2016  &nbsp;</p>
        </div>
    </div>
</nav>


<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script> var siteurl = '{$siteurl}';</script>
<script src="{$siteurl}/assets/js/main.js"></script> 

</body>
</html>