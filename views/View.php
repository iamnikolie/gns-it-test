<?php

class View {

    protected $templater;

    function __construct(){
        $this->templater = new Smarty();
        $this->views = imnCore::$sets;
    }

    public function render($tpl, $array=array()) {
        $this->templater->assign('base', __ROOT_DIR__);
        $this->templater->assign('siteurl', __URL__);

        foreach ($array as $key => $value) {
            $this->templater->assign($key, $value);
        }

        foreach ($this->views as $key => $value){
            $this->templater->assign($this->views[$key], $value);
        }
        $this->templater->assign('content', $tpl);
        $this->templater->display('layout.tpl');
    }

}