CREATE TABLE IF NOT EXISTS `films` (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  year INT,
  isActive BOOLEAN DEFAULT TRUE
);

INSERT INTO `films` (name, year) VALUES ('The Godfather', 1972);
INSERT INTO `films` (name, year) VALUES ('Matrix', 1999)