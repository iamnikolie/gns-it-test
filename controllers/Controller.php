<?php

require_once('views/View.php');

class Controller
{
    protected $view;
    protected $template;
    public $model;

    function __construct() {
        if (is_file(imnCore::$rootDir.'/models/Model.php')){
            require_once imnCore::$rootDir.'/models/Model.php';
        }

        $this->view = new View();
    }

    public function index(){}
    public function add(){}
    public function trash(){}
    public function goBack(){
        header("Location:".$_SERVER["HTTP_REFERER"]);
    }
    public function loadModel($model) {
        if (is_file(imnCore::$rootDir.'/models/'.$model.'.php')){
            require_once imnCore::$rootDir.'/models/'.$model.'.php';
        }
        $this->model = new $model;
    }
    public function getId(){
        foreach($_GET as $key => $value) {
            if ($key == 'id') {
                if (is_int((int)$value)){
                    $id = (int)$value;
                }
            }
        }
        return $id;
    }
}