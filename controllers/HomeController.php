<?php

require_once('Controller.php');

class HomeController extends Controller
{
    public function index(){
        $this->loadModel('Film');
        $this->view->render('index', [
            'films' => $this->model->all(),
        ]);
    }

    public function add(){
        $this->view->render('add');
    }

    public function trash(){
        $id = $this->getId();
        $this->loadModel('Film');
        $this->model->delete($id);
        $this->goBack();
    }

    public function store(){
        if ($_POST) {
            $array = $_POST;
            $this->loadModel('Film');
            $this->model->save($array);
            echo json_encode(array('success'=>true));
        }
    }

}