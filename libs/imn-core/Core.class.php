<?php

class imnCore {

    public static $rootDir;
    public static $sets;

    private $configures;
    private $routes;

    function __construct($conf){
        imnCore::$sets = $conf;
        $this->configures = imnCore::$sets['routes'];
        $this->routes = imnCore::$sets['routes'];
        $this->setRootDir(realpath('.'));

        if (is_file(imnCore::$rootDir.'/models/Model.php')){
            require_once imnCore::$rootDir.'/models/Model.php';
        }
        if (is_file(imnCore::$rootDir.'/models/Controller.php')){
            require_once imnCore::$rootDir.'/controllers/Controller.php';
        }
        if (is_file(imnCore::$rootDir.'/models/Controller.php')){
            require_once imnCore::$rootDir.'/views/View.php';
        }

        $this->autoload();
    }

    public function setRootDir($alias){
        imnCore::$rootDir = $alias;
    }

    public function getRoodDir() {
        return imnCore::$rootDir;
    }

    public function autoload(){
        $this->requireFromArray(imnCore::$sets['configures']['libs']);
    }

    public function load() {
        if (isset($_GET['controller']))
            $name = $_GET['controller'].'Controller';
        else $name = $this->routes['default']['controller'];

        $n = ucfirst($name);

        if (is_file(imnCore::$rootDir.'/controllers/'.$n .'.php')){
            require_once imnCore::$rootDir.'/controllers/'.$n .'.php';
            return new $n;
        } else {
            return False;
        }

    }

    public function action($action='', $class=''){
        if ($action == '' and $class == '' and !($_GET)) {
            $class = $this->routes['default']['controller'];
            $action = $this->routes['default']['action'];
        }
        if ($_GET){
            if ($_GET['controller'] != ''){
                $class = ucfirst($_GET['controller']).'Controller';
            }
            if ($_GET['action'] != '') {
                $action = $_GET['action'];
            }
        }
        $c = new $class;
        $c->{$action}();
    }

    private function read($alias){
        if ($handle = opendir(imnCore::$rootDir. '/' .$alias)){
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' and $file != '..'){
                    if (is_file(imnCore::$rootDir.'/models/'.$file)){
                        require_once imnCore::$rootDir.'/models/'.$file;
                    }
                }
            }
        }
    }

    private function requireFromArray($array){
        foreach ($array as $item) {
            if (is_file(imnCore::$rootDir.'/libs/'.$item['name'].'/'.$item['path'].'.php')){
                require_once imnCore::$rootDir.'/libs/'.$item['name'].'/'.$item['path'].'.php';
            }
        }
    }

}